import 'package:flutter/material.dart';

class Nav extends StatelessWidget{
	const Nav({super.key});
	
	@override
	Widget build(BuildContext context) {
		return Drawer(
			child: ListView(
				children: <Widget>[
					ListTile(
						leading: const Icon(Icons.change_history),
						title: const Text("Change History"),
						onTap: () {
							Navigator.pop(context);
						},
					),
				]
			), // ListView
		); // Return Drawer
	}
}
