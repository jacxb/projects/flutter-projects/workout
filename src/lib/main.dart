import 'package:flutter/material.dart';
import 'home.dart';
import 'nav.dart';

void main() {
	runApp(const MainApp());
}

class MainApp extends StatelessWidget {
 	const MainApp({super.key});

	@override
	Widget build(BuildContext context) {
		return MaterialApp(
			theme: ThemeData(useMaterial3: true),
			home: Scaffold(
				appBar: AppBar(
					title: const Text('Drawer Example'),
				),
				drawer: Nav(),
				drawerEdgeDragWidth: MediaQuery.of(context).size.width, // this will make full screen swipable for drawer
				body: Home(),
			)
		);
	}
}
